const Admin = require('../models/admin')
const mongoose = require("mongoose");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const accessToken = require('../config');

const addAdmin = function (req, res) {
  Admin.create({name: req.body.name, age: req.body.age}, function(err, doc){
    if(err) return console.log(err);
    res.send(`Сохранен объект ${doc}`);
  });
};

const getAdmins = function (req, res) {
  Admin.find({_id: req.params.id}, function(err, doc){
  if(err) return console.log(err);
    res.send(`Данные пользователя: ${doc}`);
  })
};

const deleteAdmin = (req, res) => {
  Admin.findOneAndDelete({_id: req.params.id}, function(err, doc){
    if(err) return console.log(err);
      res.send(`Пользователь удален.`);
    })
};

const putAdmin = function (req, res) {
  Admin.findByIdAndUpdate({_id: req.params.id}, {name: req.body.name, age: req.body.age}, function(err, user){
    if(err) return console.log(err);
      res.send(`Данные пользователя ${user} обновлены.`);
    })
};

const regAdmin = async (req,res) => {
      try {
          const { name, age, login, password } = req.body;
      if (!(login && password && age && name)) {
        res.status(400).send("All input is required");
      }
      const oldAdmin = await Admin.findOne({ login });
      if (oldAdmin) {
        return res.status(409).send("Admin Already Exist. Please Login");
      }
      encryptedPassword = await bcrypt.hash(password, 7);
      const admin = await Admin.create({
        name,
        age,
        login: login.toLowerCase(), 
        password: encryptedPassword,
      });
      const token = jwt.sign(
        { admin_id: admin._id, login },
          accessToken,
        {
          expiresIn: "2h",
        }
      );
      admin.token = token;
      res.status(201).json(admin);
    } catch (err) {
      console.log(err);
    }
  };

const loginAdmin = async (req, res) => {
    try {
      const { login, password } = req.body;
      if (!(login && password)) {
        res.status(400).send("All input is required");
      }
      const admin = await Admin.findOne({ login });
      if (admin && (await bcrypt.compare(password, admin.password))) {
        const token = jwt.sign(
          { admin_id: admin._id, login },
            accessToken,
          {
            expiresIn: "2h",
          }
        );
        admin.token = token;
        res.status(200).json(admin);
      }
      res.status(400).send("Invalid Credentials");
    } catch (err) {
      console.log(err);
    }
  };

module.exports = { deleteAdmin, putAdmin, getAdmins, addAdmin, regAdmin, loginAdmin };
