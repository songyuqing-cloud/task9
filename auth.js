const jwt = require('jsonwebtoken');
const accessToken = require('./config');

const verifyToken = (req, res, next) => {
  const authHeader = req.headers.authorization;
  const token = authHeader.split(' ')[1];
  if (!token) {
    return res.status(403).send("A token is required for authentication");
  }
  try {
    const decoded = jwt.verify(token, accessToken);;
    req.user = decoded;
    req.decoded = decoded;
  } catch (err) {
    return res.status(401).send("Invalid Token");
  }
  return next();
};

const isAdmin = (req, res, next) => {
  if (!req.decoded.admin_id) {
    return res.status(403).send("Forbidden")
  }
  return next();
}

module.exports = { verifyToken, isAdmin };