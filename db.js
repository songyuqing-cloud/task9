const mongoose = require("mongoose");

const db = mongoose.connect("mongodb://localhost:27017/usersdb",
    { useUnifiedTopology: true, 
        useNewUrlParser: true, 
        useFindAndModify: true })
        .then(() => {
            console.log("Successfully connected to database");
            })
        .catch((error) => {
            console.log("database connection failed. exiting now...");
            console.error(error);
            process.exit(1);
        });

module.exports = db;